# CESI-Lab-TF-VM

**Configuration de l'authentification entre Terraform et votre souscription Azure**

Vous devez créer un service principal dans Azure pour obtenir les informations d'identification nécessaires à Terraform. Cela peut être fait via le portail Azure.

- Accédez au portail Azure puis ouvrez le menu burger en haut à gauche (Vérifier que l'interface Azure est bien en anglais, ce sera plus utile pour la suite. Vous pouvez changer ce paramètre depuis votre page utilisateur et recharger la page)
- Allez dans Microsoft Entra ID > App registrations > New registration. Nommez là comme vous le souhaitez.

_Explication : L'objet que nous avons crée représente une identité au sein d'Azure que nous pouvons pouvoir utiliser pour qu'une application ou un service (en l'occurence içi un service pour Terraform). C'est à travers celle-ci que notre application va pouvoir s'authentifier et accéder aux ressources Azure_

- Notez l'Application (client) ID, Directory (tenant) . Il faut maintenant créer un secret pour ce compte de service.
- Cliquer sur votre compte de service fraichement crée et aller sur la partie "Certification et secrets" sur le menu de gauche
- Cliquer sur "New client secret", rentrer la description que vous voulez et mettez une date d'expiration suffisante.
- Créer ensuite votre secret et **notez bien sa valeur se trouvant dans la partie 'value' vous ne pourrez plus y avoir accès ensuite**

Vous avez désormais 3 informations sur 4 pour configurer votre provider Azure Terraform. Il manque votre subscription ID à trouver.
