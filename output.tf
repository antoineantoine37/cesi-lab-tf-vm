output "resource_group_name" {
  value = data.azurerm_resource_group.rg.name
}

output "vm_name" {
  value = azurerm_linux_virtual_machine.my_terraform_vm.name
}

output "public_ip_address" {
  value = azurerm_linux_virtual_machine.my_terraform_vm.public_ip_address
}