# Authentication
variable "provider_authentication" {
  type = map(string)
  default = {
    subscription_id  = "Enter your Subscription ID"
    tenant_id  = "Enter your Tenant ID"
    client_id = "Enter your Client ID"
    client_secret = "Enter your Client Secret"
  }
}

# Resource group
variable "resource_group_name" {
  type = map(string)
  default = {
    name  = "myResourceGroup"
    location  = "East US"
  }
}

# VM
variable "username" {
  type        = string
  description = "The username for the local account that will be created on the new VM."
  default     = "azureadmin"
}
variable "password" {
  type        = string
  description = "The password for the local account that will be created on the new VM."
  default     = "P@$$w0rd1234!"
}